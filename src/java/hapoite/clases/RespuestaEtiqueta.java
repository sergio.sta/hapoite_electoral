/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hapoite.clases;

import java.util.List;

/**
 *
 * @author sergi
 */
public class RespuestaEtiqueta {
   private List <Etiqueta> etiqueta;  
   private String estado;
   private String mensaje;

    /**
     * @return the etiqueta
     */
    public List <Etiqueta> getEtiqueta() {
        return etiqueta;
    }

    /**
     * @param etiqueta the etiqueta to set
     */
    public void setEtiqueta(List <Etiqueta> etiqueta) {
        this.etiqueta = etiqueta;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
