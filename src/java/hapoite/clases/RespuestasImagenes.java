/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hapoite.clases;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sergi
 */
public class RespuestasImagenes {
    /*    */   private String estado;
/*    */   private String mensaje;
/*    */   private Integer nroConsulta;
/*    */   private List<ListaImagenes> listaImagenes = new ArrayList<ListaImagenes>();

    @Override
    public String toString() {
        return "RespuestasImagenes{" + "estado=" + estado + ", mensaje=" + mensaje + ", nroConsulta=" + nroConsulta + ", listaImagenes=" + listaImagenes + '}';
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the nroConsulta
     */
    public Integer getNroConsulta() {
        return nroConsulta;
    }

    /**
     * @param nroConsulta the nroConsulta to set
     */
    public void setNroConsulta(Integer nroConsulta) {
        this.nroConsulta = nroConsulta;
    }

    /**
     * @return the listaImagenes
     */
    public List<ListaImagenes> getListaImagenes() {
        return listaImagenes;
    }

    /**
     * @param listaImagenes the listaImagenes to set
     */
    public void setListaImagenes(List<ListaImagenes> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }
}
