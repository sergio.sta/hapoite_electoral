package hapoite.clases;

import java.sql.Date;

/*     */ 
/*     */ 
/*     */ public class DetalleListaRef
/*     */ {

    @Override
    public String toString() {
        return "DetalleListaRef{" + "IMEIReferente=" + IMEIReferente + ", CodReferente=" + CodReferente + ", CedulaReferente=" + CedulaReferente + ", NombreApellidoOperador=" + NombreApellidoOperador + ", CelularReferente=" + CelularReferente + '}';
    }

    

    
    
    
/*     */   private String IMEIReferente;
/*     */   
/*     */   private String CodReferente      ;
/*     */    private String Coordinacion      ;
/*     */   private String CedulaReferente   ;
/*     */   
/*     */   private String NombreApellidoOperador;
/*     */   
/*     */ 
/*     */   private String CelularReferente   ;
 private String Blanco   ;
  private String Verde   ;
   private String Amarillo   ;
    private String Rojo   ;
     private String Total   ;
     private String CodEstamento  ;
     private String  CodComite      ;
     
     private String  TotalSufragio       ;
/*     */  

    /**
     * @return the IMEIReferente
     */
    public String getIMEIReferente() {
        return IMEIReferente;
    }

    /**
     * @param IMEIReferente the IMEIReferente to set
     */
    public void setIMEIReferente(String IMEIReferente) {
        this.IMEIReferente = IMEIReferente;
    }

    /**
     * @return the CodReferente
     */
    public String getCodReferente() {
        return CodReferente;
    }

    /**
     * @param CodReferente the CodReferente to set
     */
    public void setCodReferente(String CodReferente) {
        this.CodReferente = CodReferente;
    }

    /**
     * @return the CedulaReferente
     */
    public String getCedulaReferente() {
        return CedulaReferente;
    }

    /**
     * @param CedulaReferente the CedulaReferente to set
     */
    public void setCedulaReferente(String CedulaReferente) {
        this.CedulaReferente = CedulaReferente;
    }

  
    /**
     * @return the CelularReferente
     */
    public String getCelularReferente() {
        return CelularReferente;
    }

    /**
     * @param CelularReferente the CelularReferente to set
     */
    public void setCelularReferente(String CelularReferente) {
        this.CelularReferente = CelularReferente;
    }

    /**
     * @return the NombreApellidoOperador
     */
    public String getNombreApellidoOperador() {
        return NombreApellidoOperador;
    }

    /**
     * @param NombreApellidoOperador the NombreApellidoOperador to set
     */
    public void setNombreApellidoOperador(String NombreApellidoOperador) {
        this.NombreApellidoOperador = NombreApellidoOperador;
    }

    /**
     * @return the Blanco
     */
    public String getBlanco() {
        return Blanco;
    }

    /**
     * @param Blanco the Blanco to set
     */
    public void setBlanco(String Blanco) {
        this.Blanco = Blanco;
    }

    /**
     * @return the Verde
     */
    public String getVerde() {
        return Verde;
    }

    /**
     * @param Verde the Verde to set
     */
    public void setVerde(String Verde) {
        this.Verde = Verde;
    }

    /**
     * @return the Amarillo
     */
    public String getAmarillo() {
        return Amarillo;
    }

    /**
     * @param Amarillo the Amarillo to set
     */
    public void setAmarillo(String Amarillo) {
        this.Amarillo = Amarillo;
    }

    /**
     * @return the Rojo
     */
    public String getRojo() {
        return Rojo;
    }

    /**
     * @param Rojo the Rojo to set
     */
    public void setRojo(String Rojo) {
        this.Rojo = Rojo;
    }

    /**
     * @return the Total
     */
    public String getTotal() {
        return Total;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(String Total) {
        this.Total = Total;
    }

    /**
     * @return the Coordinacion
     */
    public String getCoordinacion() {
        return Coordinacion;
    }

    /**
     * @param Coordinacion the Coordinacion to set
     */
    public void setCoordinacion(String Coordinacion) {
        this.Coordinacion = Coordinacion;
    }

    /**
     * @return the CodEstamento
     */
    public String getCodEstamento() {
        return CodEstamento;
    }

    /**
     * @param CodEstamento the CodEstamento to set
     */
    public void setCodEstamento(String CodEstamento) {
        this.CodEstamento = CodEstamento;
    }

    /**
     * @return the CodComite
     */
    public String getCodComite() {
        return CodComite;
    }

    /**
     * @param CodComite the CodComite to set
     */
    public void setCodComite(String CodComite) {
        this.CodComite = CodComite;
    }

    /**
     * @return the TotalSufragio
     */
    public String getTotalSufragio() {
        return TotalSufragio;
    }

    /**
     * @param TotalSufragio the TotalSufragio to set
     */
    public void setTotalSufragio(String TotalSufragio) {
        this.TotalSufragio = TotalSufragio;
    }
 }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\beans\DetalleLista.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */