package hapoite.clases;

import java.sql.Date;

/*     */ 
/*     */ 
/*     */ public class DetalleLista
/*     */ {

    /**
     * @return the Vota_en_MSG_Completo
     */
    public String getVota_en_MSG_Completo() {
        return Vota_en_MSG_Completo;
    }

    /**
     * @param Vota_en_MSG_Completo the Vota_en_MSG_Completo to set
     */
    public void setVota_en_MSG_Completo(String Vota_en_MSG_Completo) {
        this.Vota_en_MSG_Completo = Vota_en_MSG_Completo;
    }

    @Override
    public String toString() {
        return "DetalleLista{" + "nombreApellidoOperador=" + nombreApellidoOperador + ", codigoOperador=" + codigoOperador + ", cedulaCorreli=" + cedulaCorreli + ", nombreApellidoCorreli=" + nombreApellidoCorreli + ", habilitado=" + habilitado + ", habilitadoMSG=" + habilitadoMSG + ", voto=" + voto + ", celular=" + celular + ", localVotacion=" + localVotacion + ", observacion1=" + observacion1 + ", observacion2=" + observacion2 + ", codigo=" + codigo + ", nroMesa=" + nroMesa + ", carnet=" + carnet + ", referencia=" + referencia + ", memo=" + memo + ", ordenMesa=" + ordenMesa + ", nroConsulta=" + nroConsulta + ", aniversario=" + aniversario + ", Voto_MSG=" + Voto_MSG + ", Certeza=" + Certeza + ", Vota_en_MSG=" + Vota_en_MSG + '}';
    }
    
    
/*     */   private String nombreApellidoOperador;
/*     */   
/*     */   private String codigoOperador;
/*     */   
/*     */   private String cedulaCorreli;
/*     */   
/*     */   private String nombreApellidoCorreli;
/*     */   
/*     */   private String habilitado;
/*     */   private String habilitadoMSG;
/*     */   private String voto;
/*     */   
/*     */   private String celular;
/*     */   
/*     */   private String localVotacion;
/*     */   private String observacion1;
            private String observacion2;
            private String codigo;
/*     */   private Integer nroMesa;
/*     */   private String carnet;
            private String referencia;
            private String memo;
/*     */   private Integer ordenMesa;
/*     */   
/*     */   private Integer nroConsulta;
            private Date aniversario;
            private String Voto_MSG       ;
            private String Certeza;
            private String Vota_en_MSG;
            private String Vota_en_MSG_Completo;
            
                        private String Certeza_Coordinador;
            private String Referencia_Coordinador;
/*     */   
/*     */   public String getNombreApellidoOperador()
/*     */   {
/*  30 */     return this.nombreApellidoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNombreApellidoOperador(String nombreApellidoOperador)
/*     */   {
/*  37 */     this.nombreApellidoOperador = nombreApellidoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCodigoOperador()
/*     */   {
/*  44 */     return this.codigoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCodigoOperador(String codigoOperador)
/*     */   {
/*  51 */     this.codigoOperador = codigoOperador;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCedulaCorreli()
/*     */   {
/*  58 */     return this.cedulaCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCedulaCorreli(String cedulaCorreli)
/*     */   {
/*  65 */     this.cedulaCorreli = cedulaCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getNombreApellidoCorreli()
/*     */   {
/*  72 */     return this.nombreApellidoCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNombreApellidoCorreli(String nombreApellidoCorreli)
/*     */   {
/*  79 */     this.nombreApellidoCorreli = nombreApellidoCorreli;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getHabilitado()
/*     */   {
/*  86 */     return this.habilitado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setHabilitado(String habilitado)
/*     */   {
/*  93 */     this.habilitado = habilitado;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getVoto()
/*     */   {
/* 100 */     return this.voto;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setVoto(String voto)
/*     */   {
/* 107 */     this.voto = voto;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCelular()
/*     */   {
/* 114 */     return this.celular;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCelular(String celular)
/*     */   {
/* 121 */     this.celular = celular;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getLocalVotacion()
/*     */   {
/* 128 */     return this.localVotacion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setLocalVotacion(String localVotacion)
/*     */   {
/* 135 */     this.localVotacion = localVotacion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Integer getNroMesa()
/*     */   {
/* 142 */     return this.nroMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNroMesa(Integer nroMesa)
/*     */   {
/* 149 */     this.nroMesa = nroMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Integer getOrdenMesa()
/*     */   {
/* 156 */     return this.ordenMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setOrdenMesa(Integer ordenMesa)
/*     */   {
/* 163 */     this.ordenMesa = ordenMesa;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Integer getNroConsulta()
/*     */   {
/* 170 */     return this.nroConsulta;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setNroConsulta(Integer nroConsulta)
/*     */   {
/* 177 */     this.nroConsulta = nroConsulta;
/*     */   }
/*     */

    public void setAniversario(Date date) {
        this.aniversario = date;
    }

    /**
     * @return the aniversario
     */
    public Date getAniversario() {
        return aniversario;
    }

    /**
     * @return the observacion1
     */
    public String getObservacion1() {
        return observacion1;
    }

    /**
     * @param observacion1 the observacion1 to set
     */
    public void setObservacion1(String observacion1) {
        this.observacion1 = observacion1;
    }

    /**
     * @return the observacion2
     */
    public String getObservacion2() {
        return observacion2;
    }

    /**
     * @param observacion2 the observacion2 to set
     */
    public void setObservacion2(String observacion2) {
        this.observacion2 = observacion2;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the carnet
     */
    public String getCarnet() {
        return carnet;
    }

    /**
     * @param carnet the carnet to set
     */
    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * @return the memo
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @param memo the memo to set
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return the habilitadoMSG
     */
    public String getHabilitadoMSG() {
        return habilitadoMSG;
    }

    /**
     * @param habilitadoMSG the habilitadoMSG to set
     */
    public void setHabilitadoMSG(String habilitadoMSG) {
        this.habilitadoMSG = habilitadoMSG;
    }

    /**
     * @return the Voto_MSG
     */
    public String getVoto_MSG() {
        return Voto_MSG;
    }

    /**
     * @param Voto_MSG the Voto_MSG to set
     */
    public void setVoto_MSG(String Voto_MSG) {
        this.Voto_MSG = Voto_MSG;
    }

    /**
     * @return the Certeza
     */
    public String getCerteza() {
        return Certeza;
    }

    /**
     * @param Certeza the Certeza to set
     */
    public void setCerteza(String Certeza) {
        this.Certeza = Certeza;
    }

    /**
     * @return the Vota_en_MSG
     */
    public String getVota_en_MSG() {
        return Vota_en_MSG;
    }

    /**
     * @param Vota_en_MSG the Vota_en_MSG to set
     */
    public void setVota_en_MSG(String Vota_en_MSG) {
        this.Vota_en_MSG = Vota_en_MSG;
    }

    /**
     * @return the Certeza_Coordinador
     */
    public String getCerteza_Coordinador() {
        return Certeza_Coordinador;
    }

    /**
     * @param Certeza_Coordinador the Certeza_Coordinador to set
     */
    public void setCerteza_Coordinador(String Certeza_Coordinador) {
        this.Certeza_Coordinador = Certeza_Coordinador;
    }

    /**
     * @return the Referencia_Coordinador
     */
    public String getReferencia_Coordinador() {
        return Referencia_Coordinador;
    }

    /**
     * @param Referencia_Coordinador the Referencia_Coordinador to set
     */
    public void setReferencia_Coordinador(String Referencia_Coordinador) {
        this.Referencia_Coordinador = Referencia_Coordinador;
    }
 }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\beans\DetalleLista.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */