/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hapoite.clases;

/**
 *
 * @author sergi
 */
public class ListaImagenes {
   /* 	CodEleccion    CHAR(8)       NOT NULL,
   Activo         CHAR(1)       NOT NULL DEFAULT 'S',  
	Descrip        VARCHAR(400)  NOT NULL,
	Documento      LONG BINARY   NULL,*/
    
    private String CodEleccion;
    private String Activo;
    private String Descrip;
    private String Documento;

    public ListaImagenes() {
        this.CodEleccion = "SIN DATOS";
        this.Activo = "N";
        this.Descrip = "SIN DATOS";
        this.Documento = "SIN DATOS";
    }

    @Override
    public String toString() {
        return "ListaImagenes{" + "CodEleccion=" + CodEleccion + ", Activo=" + Activo + ", Descrip=" + Descrip  + '}';
    }

    /**
     * @return the CodEleccion
     */
    public String getCodEleccion() {
        return CodEleccion;
    }

    /**
     * @param CodEleccion the CodEleccion to set
     */
    public void setCodEleccion(String CodEleccion) {
        this.CodEleccion = CodEleccion;
    }

    /**
     * @return the Activo
     */
    public String getActivo() {
        return Activo;
    }

    /**
     * @param Activo the Activo to set
     */
    public void setActivo(String Activo) {
        this.Activo = Activo;
    }

    /**
     * @return the Descrip
     */
    public String getDescrip() {
        return Descrip;
    }

    /**
     * @param Descrip the Descrip to set
     */
    public void setDescrip(String Descrip) {
        this.Descrip = Descrip;
    }

    /**
     * @return the Documento
     */
    public String getDocumento() {
        return Documento;
    }

    /**
     * @param Documento the Documento to set
     */
    public void setDocumento(String Documento) {
        this.Documento = Documento;
    }

    /**
     * @return the Documento
     */

}
