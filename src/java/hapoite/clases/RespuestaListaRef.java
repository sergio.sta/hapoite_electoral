package hapoite.clases;
/*    */ 
import hapoite.clases.DetalleLista;
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class RespuestaListaRef
/*    */ {
/*    */   private String estado;
/*    */   private String mensaje;
/*    */   private Integer nroConsulta;
/*    */   private List<DetalleListaRef> detalleLista;
/*    */   /*    */@Override
   public String toString()
/*    */   {
        return "RespuestaLista{" + "estado=" + estado + ", mensaje=" + mensaje + ", nroConsulta=" + nroConsulta + ", detalleLista=" + detalleLista + '}';
   }
/*    */   
/*    */ 
/*    */

/*    */
    public RespuestaListaRef()/*    */ {
        /* 23 */     this.estado = "";
        /* 24 */     this.mensaje = "";
        /* 25 */     this.detalleLista = new ArrayList();
        /*    */
    }
 public String getEstado()
/*    */   {
/* 31 */     return this.estado;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setEstado(String estado)
/*    */   {
/* 38 */     this.estado = estado;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getMensaje()
/*    */   {
/* 45 */     return this.mensaje;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setMensaje(String mensaje)
/*    */   {
/* 52 */     this.mensaje = mensaje;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public List<DetalleListaRef> getDetalleLista()
/*    */   {
/* 59 */     return this.detalleLista;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setDetalleLista(List<DetalleListaRef> detalleLista)
/*    */   {
/* 66 */     this.detalleLista = detalleLista;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public Integer getNroConsulta()
/*    */   {
/* 73 */     return this.nroConsulta;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setNroConsulta(Integer nroConsulta)
/*    */   {
/* 80 */     this.nroConsulta = nroConsulta;
/*    */   }
/*    */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\beans\RespuestaLista.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */