/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hapoite.clases;

/**
 *
 * @author sergi
 */
public class Etiqueta {
  private String  CodEleccion    ;
  private String NombreObjeto   ;
  private String Etiqueta       ;
  private String Visible        ;

    @Override
    public String toString() {
        return "Etiqueta{" + "CodEleccion=" + CodEleccion + ", NombreObjeto=" + NombreObjeto + ", Etiqueta=" + Etiqueta + ", Visible=" + Visible + '}';
    }

    /**
     * @return the CodEleccion
     */
    public String getCodEleccion() {
        return CodEleccion;
    }

    /**
     * @param CodEleccion the CodEleccion to set
     */
    public void setCodEleccion(String CodEleccion) {
        this.CodEleccion = CodEleccion;
    }

    /**
     * @return the NombreObjeto
     */
    public String getNombreObjeto() {
        return NombreObjeto;
    }

    /**
     * @param NombreObjeto the NombreObjeto to set
     */
    public void setNombreObjeto(String NombreObjeto) {
        this.NombreObjeto = NombreObjeto;
    }

    /**
     * @return the Etiqueta
     */
    public String getEtiqueta() {
        return Etiqueta;
    }

    /**
     * @param Etiqueta the Etiqueta to set
     */
    public void setEtiqueta(String Etiqueta) {
        this.Etiqueta = Etiqueta;
    }

    /**
     * @return the Visible
     */
    public String getVisible() {
        return Visible;
    }

    /**
     * @param Visible the Visible to set
     */
    public void setVisible(String Visible) {
        this.Visible = Visible;
    }
  
            
}
