/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hapoite.bd;




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BD
{
  private static Logger log = LogManager.getLogger();
  
  public Connection conectarBD(HashMap params)
    throws Exception
  {
    Class.forName("com.sybase.jdbc3.jdbc.SybDriver");
    Properties props = new Properties();
    props.put("user", (String)params.get("User DB"));
    props.put("password", (String)params.get("Pass DB"));
   // String url = "jdbc:sybase:Tds:" + params.get("IP DB") + ":" + params.get("Port DB");
    //String url = "jdbc:sybase:Tds:192.168.7.250:2622"; // paraguay
    String url = "jdbc:sybase:Tds:127.0.0.1:26400"; // francia
    
   
    this.log.info(" url [" + url + "]");
    
    
    Connection conn = DriverManager.getConnection(url, props);
    conn.setAutoCommit(false);
    return conn;
  }
  
  public void desconectarBD(Connection pConn)
    throws SQLException
  {
    pConn.close();
  }
  
 
}
