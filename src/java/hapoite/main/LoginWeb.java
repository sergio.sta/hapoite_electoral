/*     */ package hapoite.main;
/*     */ 
/*     */ import com.google.gson.Gson;
import hapoite.bd.BD;
import hapoite.clases.RespuestaComun;

import hapoite.utiles.ReadFile;

/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
          import hapoite.utils.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/loginweb")
/*     */ public class LoginWeb
/*     */ {
/*     */   private static HashMap params;
private static Logger log = LogManager.getLogger();
/*     */   
/*     */   private static String idLog;
/*  40 */   private final BD db = new BD();
/*  41 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("user") String user, @QueryParam("pass") String pass,  @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud
         )
/*     */     throws Exception
/*     */   {
/*  58 */     Connection conexion = null;
/*  59 */     RespuestaComun respuesta = new RespuestaComun();
/*     */     
/*     */     try
/*     */     {
/*     */       try
/*     */       {
/*  65 */         idLog = "1";
/*     */         
/*  67 */         ReadFile file = new ReadFile();
/*  68 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  70 */         log.error("[" + idLog + "] ACTIVACION - Error reading configuration [" + ex.getMessage() + "]");
/*  71 */         respuesta.setEstado("ERROR");
/*  72 */         respuesta.setMensaje("INTERNAL #1");
/*  73 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  76 */       log.info("[" + idLog + "] ## Activacion - Parametros ##");
/*  77 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  78 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  79 */       log.info("[" + idLog + "] " + "cedula [" + user + "]");
/*  80 */       log.info("[" + idLog + "] " + "telefono [" + pass + "]");

/*  82 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  83 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*  85 */     /*     */       
/*     */       try
/*     */       {
/*  89 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  91 */         log.error("[" + idLog + "] ACTIVACION - Error when connecting to database  [" + ex.getMessage() + "]");
/*  92 */         respuesta.setEstado("ERROR");
/*  93 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  94 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  99 */       params.put("user", user);
/* 100 */       params.put("pass", pass);
/* 101 */     
/* 102 */       params.put("lat", latitud);
/* 103 */       params.put("long", longitud);
              
/*     */       
/*     */       try
/*     */       {
/* 107 */         respuesta = ProcesarActivacion(respuesta, idLog, conexion, params);
/*     */       }
/*     */       catch (Exception ex) {
/* 110 */         respuesta.setEstado("ERROR");
/* 111 */         respuesta.setMensaje("INTERNAL #2");
/* 112 */         log.info("[" + idLog + "] ACTIVACION - Error #2[" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 124 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 126 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 130 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 116 */       if (respuesta.getEstado() == null) {
/* 117 */         respuesta.setEstado("ERROR");
/* 118 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 120 */       log.error("[" + idLog + "] ACTIVACION - Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 124 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 126 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 131 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/* 133 */     ByteArrayOutputStream baos = new ByteArrayOutputStream();
/*     */     
/*     */ 
/*     */ 
/* 137 */     Gson gson = new Gson();
/* 138 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 140 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 142 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private RespuestaComun ProcesarActivacion(RespuestaComun respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 147 */     ResultSet resultSet = null;
/* 148 */     String query = null;
/*     */     /*


CREATE FUNCTION _WebS_Activacion_ELECC   ( mCedula       char(20)    , 
                                           mNroTelefono  char(20)    , 
                                           mIMEI         char(30)    ,
                                           mPWDParams    char(20)    ,
                                           mIDEleccion   char(8)     ,
                                           mLatitud      varchar(10) , 
                                           mLongitud     varchar(10))
RETURNS Varchar(100)

*/

/*     */     try
/*     */     {
/* 152 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Login_ELECC]");
/*     */       
/* 154 */       this.stmt = conexion.createStatement();
/* 155 */       query = "SELECT dba._WebS_Login_ELECC ('" + params.get("user") + "', '" + params.get("pass")  +  "') resultado";
/*     */       
/*     */ 
/*     */ 
/* 159 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 160 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 162 */       if (resultSet.next())
/*     */       {
/* 164 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/*     */         
/* 166 */         StringTokenizer tokens = new StringTokenizer(resultSet.getString("resultado"), "#");
/*     */         
/* 168 */         respuesta.setEstado(tokens.nextToken());
/* 169 */         respuesta.setMensaje(tokens.nextToken());
/* 170 */         conexion.commit();
/*     */       }
/*     */       else {
/* 173 */         respuesta.setEstado("ERROR");
/* 174 */         respuesta.setMensaje("INTERNAL #4");
/*     */       }
resultSet.close();
/*     */     }
/*     */     catch (Exception ex) {
/* 178 */       log.error("[" + idLog_ + "] ACTIVACION - Error #5[" + ex.getMessage() + "]");
/* 179 */       respuesta.setEstado("ERROR");
/* 180 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }finally{
               try {
         this.stmt.close();
    } catch (Exception e) {
        this.log.error("error",e);
    }
               }
/* 182 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 183 */     return respuesta;
/*     */   }
/*     */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\main\Activacion.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */