/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hapoite.main;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("resources")
public class AppConfig
  extends Application
{
  @Override
  public Set<Class<?>> getClasses()
  {
    Set<Class<?>> resources = new HashSet();
    addRestResourceClasses(resources);
    return resources;
  }
  
  private void addRestResourceClasses(Set<Class<?>> resources)
  {
    resources.add(hapoite.main.Activacion.class);
    resources.add(hapoite.main.AltaCorreli.class);
    resources.add(hapoite.main.AltaCorreliV2.class);
    resources.add(hapoite.main.BajaCorreli.class);
    resources.add(hapoite.main.Cambioelecc.class);
    resources.add(hapoite.main.Cambiopwd.class);
        resources.add(hapoite.main.Consulta.class);
        resources.add(hapoite.main.ConsultaCodigoDpto.class);
        resources.add(hapoite.main.ConsultaEleccion.class);
        resources.add(hapoite.main.ConsultaEtiqueta.class);
        resources.add(hapoite.main.ConsultaImagenes.class);
        resources.add(hapoite.main.ConsultaNombre.class);
        resources.add(hapoite.main.ConsultaNombreV2.class);
        resources.add(hapoite.main.ConsultalocalVotacion.class);
        resources.add(hapoite.main.Eliminar.class);
        resources.add(hapoite.main.Lista.class);
        resources.add(hapoite.main.ListaComite.class);
        resources.add(hapoite.main.ListaCor.class);
        resources.add(hapoite.main.ListaReferente.class);
        resources.add(hapoite.main.LoginWeb.class);
        resources.add(hapoite.main.LoginWebAcciones.class);
        resources.add(hapoite.main.RegistrarOBS.class);
        resources.add(hapoite.main.RegistrarOBSCord.class);
        resources.add(hapoite.main.Test.class);
  }
}
