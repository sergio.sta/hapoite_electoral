/*     */ package hapoite.main;
/*     */ 
import hapoite.utiles.ReadFile;
/*     */ import com.google.gson.Gson;
import hapoite.bd.BD;
import hapoite.clases.RespuestaComun;
import hapoite.clases.RespuestaConsulta;
import hapoite.clases.Telefonos;

import hapoite.utils.StringUtils;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;




/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/registronotaCord")
/*     */ public class RegistrarOBSCord
/*     */ {
/*     */   private static HashMap params;

private static Logger log = LogManager.getLogger();

/*     */   
/*     */   private static String idLog;
/*  39 */   private final BD db = new BD();
/*  40 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("imei") String imei, @QueryParam("cedula") String cedula,@QueryParam("referencia") String referencia, @QueryParam("observacion") String observacion,@QueryParam("certeza") String certeza, @QueryParam("celular") String celular, @QueryParam("coordinador") String coordinador, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud)
/*     */     throws Exception
/*     */   {
/*  56 */     Connection conexion = null;
/*  57 */     RespuestaComun respuesta = new RespuestaComun();
/*     */     try
/*     */     {
/*     */       try {
/*  61 */        // idLog = log.getIdLog();
/*     */         
/*  63 */         ReadFile file = new ReadFile();
/*  64 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  66 */         log.error("[" + idLog + "] CONSULTA - Error reading configuration [" + ex.getMessage() + "]");
/*  67 */         respuesta.setEstado("ERROR");
/*  68 */         respuesta.setMensaje("INTERNAL #1");
/*  69 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  72 */       log.info("[" + idLog + "] ## Consulta - Parametros ##");
/*  73 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  74 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  75 */       log.info("[" + idLog + "] " + "cedula [" + cedula + "]");
                log.info("[" + idLog + "] " + "referencia [" + referencia + "]");
               log.info("[" + idLog + "] " + "imei [" + imei + "]");
/*  76 */       log.info("[" + idLog + "] " + "observacion [" + observacion + "]");
                log.info("[" + idLog + "] " + "certeza [" + certeza + "]");
                log.info("[" + idLog + "] " + "celular [" + celular + "]");
                log.info("[" + idLog + "] " + "coordinador [" + coordinador + "]");
/*  77 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  78 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*     */       try
/*     */       {
/*  82 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  84 */         log.error("[" + idLog + "] CONSULTA - Error when connecting to database [" + ex.getMessage() + "]");
/*  85 */         respuesta.setEstado("ERROR");
/*  86 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  87 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  92 */       params.put("cedula", cedula);
//'CI' --> CEDULA  'CN' --> 
                params.put("referencia", referencia);
                params.put("observacion", observacion);
                params.put("celular", celular);
/*  93 */       params.put("IMEI", imei);
/*  94 */       params.put("lat", latitud);
/*  95 */       params.put("long", longitud);
                params.put("certeza", certeza);
                params.put("coordinador", coordinador);
                
/*     */       
/*     */       try
/*     */       {
/*  99 */         respuesta = ProcesarActivacion(respuesta, idLog, conexion, params);
/*     */       }
/*     */       catch (Exception ex) {
/* 102 */         respuesta.setEstado("ERROR");
/* 103 */         respuesta.setMensaje("INTERNAL #2");
/* 104 */         log.info("[" + idLog + "]CONSULTA - Error #2 [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 116 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 118 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 122 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 108 */       if (respuesta.getEstado() == null) {
/* 109 */         respuesta.setEstado("ERROR");
/* 110 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 112 */       log.error("[" + idLog + "] CONSULTA - Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 116 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 118 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 123 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/* 125 */     ByteArrayOutputStream baos = new ByteArrayOutputStream();
/*     */     
/*     */ 
/*     */ 
/* 129 */     Gson gson = new Gson();
/* 130 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 132 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 134 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private RespuestaComun ProcesarActivacion(RespuestaComun respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 139 */     ResultSet resultSet = null;
/* 140 */     String query = null;
/*     */     
/*     */     try
/*     */     {
/* 144 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Actualizar_Obs_Certeza_Referente_ELECC     ]");
/*     */       
/* 146 */       this.stmt = conexion.createStatement();
/* 147 */       query = "SELECT dba._WebS_Actualizar_Obs_Certeza_Referente_ELECC     ('" + params.get("IMEI") + "', '" + params.get("coordinador") + "', '" + params.get("cedula") + "', '" + params.get("referencia")+ "', '" + params.get("observacion")+ "', '" + params.get("celular") + "', '" + params.get("certeza") + "') resultado";
/*     */       //SELECT dba._WebS_Actualizar_Obs_ELECC ('448742200000095170', 'Hola', 'K ase', '0971667740', 'undefined', '123', '321') resultado
/*     */      //[SELECT dba._WebS_Actualizar_Obs_ELECC ('448742200000095170', '2158240', 'Ref', 'Obs', '0971172990', '123', '321') resultado]
/*     */ 
/* 151 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 152 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 154 */       if (resultSet.next())
/*     */       {
/* 156 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/*     */         
/* 158 */         StringTokenizer tokens = new StringTokenizer(resultSet.getString("resultado"), "#");
/*     */         
/* 160 */         respuesta.setEstado(tokens.nextToken());
/* 161 */         if (!respuesta.getEstado().equals("OK")) {
/* 162 */           respuesta.setMensaje(tokens.nextToken());
/*     */         }
/*     */         else {
/* 165 */           respuesta.setMensaje("EXITO");
/* 166 */          // respuesta.setEstado(query);
/*     */         }
/*     */         
/* 180 */         conexion.commit();
/*     */       }
/*     */       else {
/* 183 */         respuesta.setEstado("ERROR");
/* 184 */         respuesta.setMensaje("INTERNAL #4");
/*     */       }
resultSet.close();
/*     */     }
/*     */     catch (Exception ex) {
/* 188 */       log.error("[" + idLog_ + "] CONSULTA - Error #5 [" + ex.getMessage() + "]");
/* 189 */       respuesta.setEstado("ERROR");
/* 190 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }finally{
               try {
         this.stmt.close();
    } catch (Exception e) {
        this.log.error("error",e);
    }
               }
/* 192 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 193 */     return respuesta;
/*     */   }
/*     */ }

