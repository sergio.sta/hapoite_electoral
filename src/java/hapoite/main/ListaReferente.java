/*     */ package hapoite.main;
/*     */ 
import hapoite.utiles.ReadFile;
import hapoite.clases.DetalleLista;
/*     */ import com.google.gson.Gson;
import hapoite.bd.BD;
import hapoite.clases.DetalleListaRef;
import hapoite.clases.RespuestaLista;
import hapoite.clases.RespuestaListaRef;

import hapoite.utils.StringUtils;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
/*     */ import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;




/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/listaReferente")
/*     */ public class ListaReferente
/*     */ {
/*     */   private static HashMap params;
private static Logger log = LogManager.getLogger();
/*     */   
/*     */   private static String idLog;
/*  37 */   private final BD db = new BD();
/*  38 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
         
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("imei") String imei, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud)
/*     */     throws Exception
/*     */   {
/*  53 */     Connection conexion = null;
/*  54 */     RespuestaListaRef respuesta = new RespuestaListaRef();
/*  55 */     String rPrepararLista = "";
/*     */     try
/*     */     {
/*     */       try {
/*  59 */         idLog = "2";
/*     */         
/*  61 */         ReadFile file = new ReadFile();
/*  62 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  64 */         log.error("[" + idLog + "] LISTA - Error reading configuration [" + ex.getMessage() + "]");
/*  65 */         respuesta.setEstado("ERROR");
/*  66 */         respuesta.setMensaje("INTERNAL #1");
/*  67 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  70 */       log.info("[" + idLog + "] ## Consulta - Parametros ##");
/*  71 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  72 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  73 */       log.info("[" + idLog + "] " + "imei [" + imei + "]");
/*  74 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  75 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*     */       try
/*     */       {
/*  79 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  81 */         log.error("[" + idLog + "] LISTA - Error when connecting to database [" + ex.getMessage() + "]");
/*  82 */         respuesta.setEstado("ERROR");
/*  83 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  84 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  89 */       params.put("IMEI", imei);
/*  90 */       params.put("lat", latitud);
/*  91 */       params.put("long", longitud);
/*     */       try
/*     */       {
/*  94 */         rPrepararLista = PrepararLista(idLog, conexion, params);
/*  95 */         StringTokenizer tokens = new StringTokenizer(rPrepararLista, "#");
/*  96 */         respuesta.setEstado(tokens.nextToken());
/*  97 */         respuesta.setNroConsulta(Integer.valueOf(Integer.parseInt(tokens.nextToken())));
/*     */       } catch (Exception ex) {
/*  99 */         respuesta.setEstado("ERROR");
/* 100 */         respuesta.setMensaje("INTERNAL #2");
/* 101 */         log.info("[" + idLog + "] LISTA - Error en la lista #2[" + ex.getMessage() + "]");
/*     */       }
/*     */       
/* 104 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */       
/* 106 */       if (respuesta.getEstado().equals("OK")) {
/* 107 */         respuesta.setMensaje("EXISTE");
/*     */         try {
/* 109 */           respuesta = VerLista(respuesta, idLog, conexion);
/*     */         } catch (Exception ex) {
/* 111 */           respuesta.setEstado("ERROR");
/* 112 */           respuesta.setMensaje("INTERNAL #2");
/* 113 */           log.info("[" + idLog + "] LISTA - Error #2 [" + ex.getMessage() + "]");
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 128 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 130 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 134 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 120 */       if (respuesta.getEstado() == null) {
/* 121 */         respuesta.setEstado("ERROR");
/* 122 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 124 */       log.error("[" + idLog + "] LISTA - Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 128 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 130 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 135 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/* 137 */     ByteArrayOutputStream baos = new ByteArrayOutputStream();
/*     */     
/*     */ 
/*     */ 
/* 141 */     Gson gson = new Gson();
/* 142 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 144 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 146 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private String PrepararLista(String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 151 */     ResultSet resultSet = null;
/* 152 */     String query = null;
/* 153 */     String resultado = "";
/*     */     
/*     */     try
/*     */     {
/* 157 */       log.info("[" + idLog_ + "] Preparando la funcion [dba. _WebS_Lista_IMEI   ]");
/*     */       
/* 159 */       this.stmt = conexion.createStatement();
/* 160 */       query = "SELECT dba._WebS_Lista_IMEI    ('" + params.get("IMEI") + "') resultado";
/*     */       
/* 162 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 163 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 165 */       if (resultSet.next()) {
/* 166 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/* 167 */         resultado = resultSet.getString("resultado");
/* 168 */         conexion.commit();
/*     */       }
/*     */       else {
/* 171 */         resultado = "ERROR#INTERNAL #4";
/*     */       }
resultSet.close();
/*     */     }
/*     */     catch (Exception ex) {
/* 175 */       log.error("[" + idLog_ + "] LISTA - Error en el Procedimiento #5 [" + ex.getMessage() + "]");
/* 176 */       resultado = "ERROR#INTERNAL #5";
/*     */     }finally{
               try {
         this.stmt.close();
    } catch (Exception e) {
        this.log.error("error",e);
    }
             
}
/* 178 */     log.info("[" + idLog_ + "] respuesta [" + resultado + "]");
/* 179 */     return resultado;
/*     */   }
/*     */   
/*     */   public RespuestaListaRef VerLista(RespuestaListaRef respuesta, String idLog_, Connection conexion)
/*     */   {
/*     */     try
                  
/*     */     {
/* 186 */       this.stmt = conexion.createStatement();
/* 187 */       String query = "Select \n" +
"IMEIReferente  IMEIReferente ,\n" +
"NombresReferente  NombresReferente ,\n" +
"ApellidosReferente  ApellidosReferente ,\n" +
"Coordinacion  Coordinacion ,\n" +
"Linea Linea, \n" +
"CedulaReferente CedulaReferente ,\n" +
"CelularReferente CelularReferente ,\n" +
"CodReferente CodReferente,\n" +
"Certeza_Blanco Certeza_Blanco,\n" +
"Certeza_Verde Certeza_Verde,\n" +
"Certeza_Amarillo Certeza_Amarillo,\n" +
"Certeza_Rojo Certeza_Rojo,\n" +
"Certeza_total Certeza_total,\n" +
"TotalSufragio  TotalSufragio \n" +
"from dba.WebS_ConsultaIMEI where ConsultaNro ="+respuesta.getNroConsulta()+" \n" +
"union \n" +
"SELECT \n" +
"'111', \n" +
"'Total ' ,\n" +
"Coordinacion,\n" +
"Coordinacion ,\n" +
"999, \n" +
"'',\n" +
"'#99999',\n" +
"'CodReferente' , \n" +
"sum(Certeza_blanco)  ,\n" +
"sum(Certeza_verde)  ,\n" +
"sum(Certeza_amarillo)  ,\n" +
"sum(Certeza_rojo)  ,\n" +
"sum(Certeza_total)  ,\n" +
"sum(TotalSufragio) \n" +
"from dba.WebS_ConsultaIMEI \n" +
"where  Coordinacion != 'ZZZZ' and ConsultaNro = "+respuesta.getNroConsulta()+" group by Coordinacion\n" +
"Order by 4, 5;";
/*     */      
/* 190 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 191 */       ResultSet resultSet = this.stmt.executeQuery(query);
/*     */       
/* 193 */       if (resultSet.next()) {
/*     */         do {
/* 195 */           DetalleListaRef detalleLista = new DetalleListaRef();
/* 196 */           detalleLista.setIMEIReferente(resultSet.getString("IMEIReferente"));
/* 197 */           detalleLista.setNombreApellidoOperador(this.StringUtils.ReplaceCE(resultSet.getString("NombresReferente") + " " + resultSet.getString("ApellidosReferente")));
                    detalleLista.setCoordinacion(resultSet.getString("Coordinacion"));
/* 198 */           detalleLista.setCedulaReferente (resultSet.getString("CedulaReferente"));
                      detalleLista.setCelularReferente(resultSet.getString("CelularReferente"));
/* 199 */           detalleLista.setCodReferente(resultSet.getString("CodReferente"));
                    detalleLista.setBlanco(resultSet.getString("Certeza_Blanco"));
/* 201 */           detalleLista.setVerde(resultSet.getString("Certeza_Verde"));
                    detalleLista.setAmarillo(resultSet.getString("Certeza_Amarillo"));
/* 206 */           detalleLista.setRojo(resultSet.getString("Certeza_Rojo"));
                    detalleLista.setTotal(resultSet.getString("Certeza_total"));
                    detalleLista.setTotalSufragio(resultSet.getString("TotalSufragio"));
                    
                    /*
                    Certeza_total   ,  
Certeza_Blanco  ,  
Certeza_Verde   ,  
Certeza_Amarillo,  
Certeza_Rojo     
                    */
                    respuesta.getDetalleLista().add(detalleLista);
/* 207 */         } while (resultSet.next());
            
                    
/*     */       }
/*     */       else {
/* 210 */         respuesta.setEstado("ERROR");
/* 211 */         respuesta.setMensaje("NO EXISTEN DATOS");
/*     */       }
                
                    
                 resultSet.close();
/*     */     }
/*     */     catch (Exception ex) {
/* 215 */       log.error("[" + idLog_ + "] LISTA - Error en el Procedimiento #5 [" + ex.getMessage() + "]");
/* 216 */       respuesta.setEstado("ERROR");
/* 217 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }finally{
               try {
         this.stmt.close();
    } catch (Exception e) {
        this.log.error("error",e);
    }
               }
               ///ORDENAR PQ ASI QUIERE RAMIRO
         
    
/* 219 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 220 */     return respuesta;
/*     */   }
/*     */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\main\Lista.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */