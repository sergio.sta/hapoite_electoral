/*     */ package hapoite.main;
/*     */ 
import hapoite.utiles.ReadFile;
/*     */ import com.google.gson.Gson;
import hapoite.bd.BD;
import hapoite.clases.RespuestaComun;

import hapoite.utils.StringUtils;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;





/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/altacorreliV2")
/*     */ public class AltaCorreliV2
/*     */ {
/*     */   private static HashMap params;
private static Logger log = LogManager.getLogger();
/*     */   
/*     */   private static String idLog;
/*  40 */   private final BD db = new BD();
/*  41 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("cedula") String cedula, @QueryParam("telefono") String telefono,@QueryParam("referencia") String referencia,@QueryParam("memo") String memo,@QueryParam("certeza") String certeza, @QueryParam("imei") String imei, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud)
/*     */     throws Exception
/*     */   {
/*  58 */     Connection conexion = null;
/*  59 */     RespuestaComun respuesta = new RespuestaComun();
/*     */     try
/*     */     {
/*     */       try {
/*  63 */         idLog = "1";
/*     */         
/*  65 */         ReadFile file = new ReadFile();
/*  66 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  68 */         log.error("[" + idLog + "] ALTACORRELI - Error reading configuration [" + ex.getMessage() + "]");
/*  69 */         respuesta.setEstado("ERROR");
/*  70 */         respuesta.setMensaje("INTERNAL #1");
/*  71 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  74 */       log.info("[" + idLog + "] ## Activacion - Parametros ##");
/*  75 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  76 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  77 */       log.info("[" + idLog + "] " + "cedula [" + cedula + "]");
/*  78 */       log.info("[" + idLog + "] " + "telefono [" + telefono + "]");
log.info("[" + idLog + "] " + "referencia [" + referencia + "]");
log.info("[" + idLog + "] " + "memo [" + memo + "]");
log.info("[" + idLog + "] " + "certeza [" + certeza + "]");
/*  79 */       log.info("[" + idLog + "] " + "imei [" + imei + "]");
/*  80 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  81 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*     */       try
/*     */       {
/*  85 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  87 */         log.error("[" + idLog + "] ALTACORRELI - Error when connecting to database [" + ex.getMessage() + "]");
/*  88 */         respuesta.setEstado("ERROR");
/*  89 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  90 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  95 */       params.put("Cedula", cedula);
/*  96 */       params.put("NroTelefono", telefono);
/*  97 */       params.put("IMEI", imei);
                params.put("referencia", referencia);
                 params.put("memo", memo);
                  params.put("certeza", certeza);
/*  98 */       params.put("lat", latitud);
/*  99 */       params.put("long", longitud);
/*     */       
/*     */       try
/*     */       {
/* 103 */         respuesta = ProcesarActivacion(respuesta, idLog, conexion, params);
/*     */       }
/*     */       catch (Exception ex) {
/* 106 */         respuesta.setEstado("ERROR");
/* 107 */         respuesta.setMensaje("INTERNAL #2");
/* 108 */         log.info("[" + idLog + "] ALTACORRELI - Error #2[" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 120 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 122 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 126 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 112 */       if (respuesta.getEstado() == null) {
/* 113 */         respuesta.setEstado("ERROR");
/* 114 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 116 */       log.error("[" + idLog + "] ALTACORRELI - Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 120 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 122 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 127 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 133 */     Gson gson = new Gson();
/* 134 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 136 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 138 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private RespuestaComun ProcesarActivacion(RespuestaComun respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 143 */     ResultSet resultSet = null;
/* 144 */     String query = null;
/*     */     
/*     */     try
/*     */     {
/* 148 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Alta_ELECC_Obs ]");
/*     */       
/* 150 */       this.stmt = conexion.createStatement();
/* 151 */       query = "SELECT dba._WebS_Alta_ELECC_Obs  ('" + params.get("IMEI") + "', '" + params.get("Cedula") + "', '" + params.get("NroTelefono") + "', '" + params.get("referencia") +"', '" + params.get("memo") + "', '" + params.get("certeza") +  "', '" + params.get("lat") + "', '" + params.get("long") + "') resultado";
/*     */       
/*     */ 
/*     */ 
/* 155 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 156 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 158 */       if (resultSet.next())
/*     */       {
/* 160 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/*     */         
/* 162 */         StringTokenizer tokens = new StringTokenizer(resultSet.getString("resultado"), "#");
/*     */         
/* 164 */         respuesta.setEstado(tokens.nextToken());
/* 165 */         respuesta.setMensaje(this.StringUtils.ReplaceCE(tokens.nextToken()));
/* 166 */         conexion.commit();
/*     */       }
/*     */       else {
/* 169 */         respuesta.setEstado("ERROR");
/* 170 */         respuesta.setMensaje("INTERNAL #4");
/*     */       }
resultSet.close();
/*     */     }
/*     */     catch (Exception ex) {
/* 174 */       log.error("[" + idLog_ + "] ALTACORRELI - Error  #5[" + ex.getMessage() + "]");
/* 175 */       respuesta.setEstado("ERROR");
/* 176 */       respuesta.setMensaje("INTERNAL #5");
/*     */     } finally{
               try {
         this.stmt.close();
    } catch (Exception e) {
        this.log.error("error",e);
    }
             
}
/* 178 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 179 */     return respuesta;
/*     */   }
/*     */ }


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\main\AltaCorreli.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */