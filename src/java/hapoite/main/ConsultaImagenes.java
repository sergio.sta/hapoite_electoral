/*     */ package hapoite.main;
/*     */ 
import hapoite.utiles.ReadFile;
/*     */ import com.google.gson.Gson;

import hapoite.bd.BD;
import hapoite.clases.DetalleLista;
import hapoite.clases.ListaImagenes;
import hapoite.clases.RespuestaComun;
import hapoite.clases.RespuestaConsulta;
import hapoite.clases.RespuestaEleccion;
import hapoite.clases.RespuestasImagenes;
import hapoite.clases.Telefonos;

import hapoite.utils.StringUtils;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;




/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/consultaImagenes")
/*     */ public class ConsultaImagenes
/*     */ {
/*     */   private static HashMap params;

private static Logger log = LogManager.getLogger();

/*     */   
/*     */   private static String idLog;
/*  39 */   private final BD db = new BD();
/*  40 */   private final StringUtils StringUtils = new StringUtils();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("eleccion") String eleccion, @QueryParam("imei") String imei, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud)
/*     */     throws Exception
/*     */   {
/*  56 */     Connection conexion = null;
/*  57 */     RespuestasImagenes respuesta = new RespuestasImagenes();
/*     */     try
/*     */     {
/*     */       try {
/*  61 */        // idLog = log.getIdLog();
/*     */         
/*  63 */         ReadFile file = new ReadFile();
/*  64 */         params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  66 */         log.error("[" + idLog + "] CONSULTA - Error reading configuration [" + ex.getMessage() + "]");
/*  67 */         respuesta.setEstado("ERROR");
/*  68 */         respuesta.setMensaje("INTERNAL #1");
/*  69 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  72 */       log.info("[" + idLog + "] ## Consulta - Parametros ##");
/*  73 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  74 */       log.info("[" + idLog + "] " + "password [" + password + "]");
/*  75 */       log.info("[" + idLog + "] " + "eleccion [" + eleccion + "]");
/*  76 */       log.info("[" + idLog + "] " + "imei [" + imei + "]");
/*  77 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  78 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*     */       try
/*     */       {
/*  82 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  84 */         log.error("[" + idLog + "] CONSULTA - Error when connecting to database [" + ex.getMessage() + "]");
/*  85 */         respuesta.setEstado("ERROR");
/*  86 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  87 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  92 */       params.put("eleccion", eleccion);
/*  93 */       params.put("IMEI", imei);
/*  94 */       params.put("lat", latitud);
/*  95 */       params.put("long", longitud);
/*     */       
/*     */       try
/*     */       {
/*  99 */         respuesta = ProcesarActivacion(respuesta, idLog, conexion, params);
/*     */       }
/*     */       catch (Exception ex) {
/* 102 */         respuesta.setEstado("ERROR");
/* 103 */         respuesta.setMensaje("INTERNAL #2");
/* 104 */         log.info("[" + idLog + "]CONSULTA - Error #2 [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 116 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 118 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 122 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 108 */       if (respuesta.getEstado() == null) {
/* 109 */         respuesta.setEstado("ERROR");
/* 110 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 112 */       log.error("[" + idLog + "] CONSULTA - Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 116 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 118 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 123 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/* 125 */     ByteArrayOutputStream baos = new ByteArrayOutputStream();
/*     */     
/*     */ 
/*     */ 
/* 129 */     Gson gson = new Gson();
/* 130 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 132 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 134 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private RespuestasImagenes ProcesarActivacion(RespuestasImagenes respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 139 */     try
/*     */     {
/* 186 */       this.stmt = conexion.createStatement();
/* 187 */       String query = "SELECT * from dba.Galeria  where CodEleccion  = '" + params.get("eleccion")+"';";
/*     */       
/*     */ 
/* 190 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 191 */       ResultSet resultSet = this.stmt.executeQuery(query);
/*     */       
/* 193 */       if (resultSet.next()) {
/*     */         do {
                    log.info(resultSet.getString("Activo"));
                    log.info(resultSet.getString("Descrip"));
                    if(!(resultSet.getString("Documento")!=null)) continue;//si guardan mal
                    log.info(resultSet.getString("Documento").substring(0,10));
                    
                    if(resultSet.getString("Activo").equalsIgnoreCase("N")) continue; //no se usa
                    ListaImagenes listaImagenes = new ListaImagenes();
                    listaImagenes.setDescrip(resultSet.getString("Descrip"));
                    String doc = "sin datos" ;
                    try{
                        Base64 codec = new Base64();
                   // doc = Base64.encode(resultSet.getBytes("Documento"));
                   doc =  codec.encodeBase64String(resultSet.getBytes("Documento"));
                    }catch(Exception e){
                        log.error("no parsea",e);
                    }
                    listaImagenes.setDocumento(doc);
/* 195 */           respuesta.getListaImagenes().add(listaImagenes);
/* 206 */           
/* 207 */         } while (resultSet.next());
                    respuesta.setEstado("OK");
/* 211 */         respuesta.setMensaje("DATOS");
/*     */       }
/*     */       else {
/* 210 */         respuesta.setEstado("ERROR");
/* 211 */         respuesta.setMensaje("NO EXISTEN DATOS");
/*     */       }
resultSet.close();
/*     */     }
/*     */     catch (Exception ex) {
/* 215 */       log.error("[" + idLog_ + "] LISTA - Error en el llamado #5 [" + ex.getMessage() + "]",ex);
/* 216 */       respuesta.setEstado("ERROR");
/* 217 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }finally{
               try {
         this.stmt.close();
    } catch (Exception e) {
        this.log.error("error",e);
    }
             
}
/* 219 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 220 */     return respuesta;
/*     */   }
/*     */ }

